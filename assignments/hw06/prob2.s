# prob2.s
    .set noreorder
    .data
A:  .word 2
    .text
    .globl main
    .ent main
main:
        lui     $t1, %hi(A)
        addi    $t0, $0, 1
        ori     $t1, $t1, %lo(A)
        lw      $t2, 0($t1)
        addi    $t3, $0, 5
        bne     $t0, $t2, L
        ori     $v0, $0, 10     # exit
        addi    $t3, $0, 7
L:
        add     $t4, $t0, $t3

# Do not edit below this line
        nop
        nop
        nop
        syscall
    .end main
